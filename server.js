require('dotenv').config({
  path: `app/config/${process.env.NODE_ENV}/.env`
})
const express = require('express')
const bodyParser = require('body-parser')
const logger = require('morgan')
const pino = require('pino')()
const redis = require('./app/utils/databases/redis')
const database = require('./app/utils/databases/sql')

const app = express()

let client = redis.connectToRedis()

database.sync({ force: process.env.NODE_ENV === 'development' }).then(() => {
  pino.info('Table has been created')
})

client.on('error', (err) => {
  pino.error('Redis Error : ' + err)
})

client.on('connect', () => {
  pino.info('Connected to Redis Server.')
})

app.use(logger('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use((req, res, next) => {
  res.redisClient = client
  next()
})

const router = express.Router()
const routes = require('./app/routes')(router)
app.use('/api/v1/', routes)

const port = process.env.PORT || 3000

const server = app.listen(port, err => {
  if (!err) {
    pino.info(`${process.env.APP_NAME} Engine is listening on port : ${port}`)
  } else {
    pino.error(err)
  }
})

module.exports = server
