function showAPIVersion (req, res) {
  return res.json({
    'version': '1.0.0'
  })
}

module.exports = showAPIVersion
