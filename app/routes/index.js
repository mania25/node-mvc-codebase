const controllers = require('../controllers')

module.exports = function (app) {
  app.get('/', controllers.showAPIVersion)
  return app
}
