const redis = require('redis')
let exportsModule = (module.exports = {})

exportsModule.connectToRedis = function () {
  let redisOpt = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    db: process.env.REDIS_DB
  }

  if (process.env.REDIS_PASSWORD !== null || process.env.REDIS_PASSWORD !== '') {
    redisOpt.password = process.env.REDIS_PASSWORD
  }

  return redis.createClient()
}
