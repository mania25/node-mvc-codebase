const Sequelize = require('sequelize')
const pino = require('pino')()

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_ADAPTER,
    operatorsAliases: false
  }
)

sequelize
  .authenticate()
  .then(() => {
    pino.info(`Connection to ${process.env.DB_NAME} has been established successfully.`)
  })
  .catch(err => {
    pino.error(`Unable to connect to the database: ${err}`)
  })

module.exports = sequelize
