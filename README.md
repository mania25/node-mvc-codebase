# Codebase

# Requirement
- Node 8+
- npm 5.++

# Design Pattern : 
- M(odel)V(iew)C(ontroller)
- Model : object yang digunakan oleh controller
- Controller : logic untuk API
- View : JSON object response

# Directory Explanation.
- config: folder where all application configuration are stored.
- controllers: folder where all business logic for the application stored.
- models: folder that stored only database scheme object.
- routes: folder where all application routes are defined.
- rules: folder where all business logic rule are placed.
- utils: folder where all additional logics (like helper, validation function, etc) are stored.
- views: folder where default response object is stored.